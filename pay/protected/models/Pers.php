<?php

/**
 * This is the model class for table "pers".
 *
 * The followings are the available columns in table 'pers':
 * @property integer $id
 * @property string $name
 * @property string $address
 * @property string $ifns
 * @property string $date
 * @property string $date_end
 * @property string $phone
 * @property integer $pass_seria
 * @property integer $pass_num
 * @property integer $cid
 * @property integer $del
 */
class Pers extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pers';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, address, ifns, date, date_end, phone, cid', 'required'),
			array('cid, del', 'numerical', 'integerOnly'=>true),
			array('name, address, ifns, phone, type, doc_num, nationality, patent_num, region, comment', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, region, address, patent_num, ifns, date, comment, date_end, type, doc_num, nationality, phone, pass_seria, pass_num, cid, del', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'ФИО',
			'address' => 'АДРЕС РЕГИСТРАЦИИ',
			'ifns' => 'КОД ИФНС',
			'date' => 'ДАТА ВЫДАЧИ ПАТЕНТА',
			'date_end' => 'СРОК ОКОНЧАНИЯ ПАТЕНТА',
			'phone' => 'ТЕЛЕФОН',
			'cid' => 'КОМПАНИЯ',
            'del' => 'Del',
            'region' => 'РЕГИОН',
            'patent_num' => 'НОМЕР ПАТЕНТА',
            'type' => 'ТИП ДОКУМЕНТА',
            'doc_num' => 'НОМЕР ДОКУМЕНТА',
            'nationality' => 'ГРАЖДАНСТВО',
            'comment' => 'КОММЕНТАРИЙ'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('ifns',$this->ifns,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('date_end',$this->date_end,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('cid',$this->cid);
		$criteria->compare('del',$this->del);
		$criteria->compare('region',$this->region);
		$criteria->compare('patent_num',$this->patent_num);
		$criteria->compare('type',$this->type);
		$criteria->compare('doc_num',$this->doc_num);
		$criteria->compare('nationality',$this->nationality);
		$criteria->compare('comment',$this->comment);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getCompanyName($id){
        $comapny = Companies::model()->findByPk($id);
        if(!empty($comapny->company_name))
            return $comapny->company_name;
        else
            return '';
    }

    public function getUserCompany(){
        $c = Accounts::model()->findByPk(Yii::app()->user->getId());
        if($c)
            return $c->cid;
        else
            return false;
    }

    public function getUserCompanyName(){
        $c = Accounts::model()->findByPk(Yii::app()->user->getId());
        if(!empty($c->cid)){
            $d = Companies::model()->findByPk($c->cid);
            if(!empty($d->company_name))
                return $d->company_name;
            else
                return '';
        }else{
            return '';
        }

    }

    public function getCount(){
     //подсчитаем количество завершающихся и завершенных патентов
            $cid = $this->getUserCompany();
            if(!empty($cid)){
                $count['r'] = Pers::model()->count('date_end >= DATE_SUB(CURDATE(), INTERVAL 5 DAY) AND date_end <= CURDATE() AND cid ='.$cid);
                $count['e'] = Pers::model()->count('date_end > CURDATE() AND cid ='.$cid);
                return $count;
            }else{
                return false;
            }
    }


    public function getTypeName($id){
        if(!empty($id)){
            $c = Docs::model()->find("code =".$id);
            if(!empty($c->name))
                return $id.'-'.$c->name;
            else
                return '';
        }
        return '';
    }

    public function getNationalName($id){
        if(!empty($id)){
            $c = Countries::model()->findByPk($id);
            if(!empty($c->name))
                return $id.'-'.$c->name;
            else
                return '';
        }
        return '';
    }

    public function getPayDate($id){

        $pers = Pers::model()->findByPk($id);

        $date = strtotime("+1 month", strtotime($pers->date_end));
        $date = strtotime("-1 day", $date);
        return date("d-m-Y", $date);
    }


}
