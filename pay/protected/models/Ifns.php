<?php

/**
 * This is the model class for table "ifns".
 *
 * The followings are the available columns in table 'ifns':
 * @property string $name
 * @property string $fullname
 * @property integer $kpp
 * @property integer $rek
 * @property integer $inn
 * @property string $address
 * @property string $phone
 * @property string $mail
 * @property integer $okpo
 * @property string $bank
 * @property integer $bik
 * @property integer $korr
 * @property integer $num
 * @property integer $code
 */
class Ifns extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ifns';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, fullname, kpp, rek, inn, address, phone, mail, okpo, bank, bik, korr, num, code', 'required'),
			array('kpp, rek, inn, okpo, bik, korr, num, code', 'numerical', 'integerOnly'=>true),
			array('name, fullname, address, phone, mail, bank', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('code, name, fullname, kpp, rek, inn, address, phone, mail, okpo, bank, bik, korr, num', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'name' => 'Наименование',
			'fullname' => 'Получатель платежа',
			'code' => 'Код',
			'kpp' => 'КПП',
			'rek' => 'Rek',
			'inn' => 'ИНН',
			'address' => 'Address',
			'phone' => 'Phone',
			'mail' => 'Mail',
			'okpo' => 'ОКПО',
			'bank' => 'Банк получателя',
			'bik' => 'БИК',
			'korr' => 'Корр. счет №',
			'num' => 'Счет №',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('name',$this->name,true);
		$criteria->compare('fullname',$this->fullname,true);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('kpp',$this->kpp);
		$criteria->compare('rek',$this->rek);
		$criteria->compare('inn',$this->inn);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('mail',$this->mail,true);
		$criteria->compare('okpo',$this->okpo);
		$criteria->compare('bank',$this->bank,true);
		$criteria->compare('bik',$this->bik);
		$criteria->compare('korr',$this->korr);
		$criteria->compare('num',$this->num);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Ifns the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
