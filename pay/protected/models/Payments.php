<?php

/**
 * This is the model class for table "payments".
 *
 * The followings are the available columns in table 'payments':
 * @property integer $id
 * @property integer $pers_id
 * @property integer $sum
 * @property string $date
 * @property string $exported
 * @property string $payed_till
 */
class Payments extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'payments';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pers_id, sum, date, payed_till', 'required'),
			array('pers_id, sum', 'numerical', 'integerOnly'=>true),
			array('exported, payed_till', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, pers_id, sum, date, exported, payed_till', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'pers_id' => 'Пользователь',
			'sum' => 'Сумма',
			'date' => 'Дата',
			'exported' => 'Exported',
			'payed_till' => 'Оплачено до',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('pers_id',$this->pers_id);
		$criteria->compare('sum',$this->sum);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('exported',$this->exported,true);
		$criteria->compare('payed_till',$this->payed_till,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Payments the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getUserName($id){
        $pers = Pers::model()->findByPk($id);
          if(!empty($pers->name))
                return $pers->name;
            else
                return '';
    }

    public function getUserCompany(){
        $c = Accounts::model()->findByPk(Yii::app()->user->getId());
        return $c->cid;
    }
}
