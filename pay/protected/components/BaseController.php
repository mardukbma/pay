<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 12.08.2015
 * Time: 14:15
 */

class BaseController extends Controller{

    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action))
        {
            if(Yii::app()->user->isGuest){
                $this->redirect(Yii::app()->createUrl('site/login'));
            }
            return true;
        }
        else
            return false;
    }
} 