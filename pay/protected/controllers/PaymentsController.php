<?php

class PaymentsController extends BaseController
{
    private $_model;
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

    public function loadModel($id = 0)
    {
        if($this->_model===null)
        {
            if(!empty($id)){
                $condition = 'id = ' . $id;
            }
            $this->_model=Payments::model()->find(array(), $condition);

            if($this->_model===null)
                throw new CHttpException(404,'Запрашиваемая страница не существует.');
        }
        return $this->_model;
    }

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex($id=0)
	{
        $criteria = new CDbCriteria();

        $cid = Pers::model()->getUserCompany();

        if(!empty($id)){
            $criteria->condition = 't.pers_id ='.$id;
        }else{
            if(!empty($cid)){
                $criteria->select = 't.*';
                $criteria->join = 'LEFT JOIN pers pers ON t.pers_id = pers.id ';
                $criteria->join .= 'LEFT JOIN accounts acc ON pers.cid = acc.cid ';

                $criteria->condition = 'acc.cid ='. $cid;
                $criteria->order = 't.date ASC';
            }

        }

        $pages = new CPagination(Payments::model()->count());
        $pages->pageSize = 10;
        $pages->applyLimit($criteria);

        $dataProvider=new CActiveDataProvider('Payments', array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 10,
            ),

        ));

        $this->render('payments', array('dataProvider'=>$dataProvider, 'pages' => $pages));
	}

    public function actionDelete($id)
    {
        $model = $this->loadModel($id);
        if($model===null)
            throw new CHttpException(404,'Запрашиваемая страница не существует.');

        $model->delete();
        $this->actionIndex();
        Yii::app()->end();

    }

    public function actionUpdate($id = 0)
    {
        if(!empty($id)){
            $model = $this->loadModel($id);
        }else{
            $model = new Payments();
        }

        if (isset($_POST['Payments'])) {
            $model->attributes = $_POST['Payments'];

            $model->date = date ('Y-m-d', strtotime($model->date));

            $date = strtotime("+1 month", strtotime($_POST['Payments']['date']));
            $date = strtotime("-1 day", $date);
            $model->payed_till = date("Y-m-d", $date);
            if ($model->validate()) {
                if ($model->save()) {
                    $this->actionIndex();
                    Yii::app()->end();
                }
            }

        }

        $model->date = date ('d-m-Y', strtotime($model->date));

        $pers = Pers::model()->findAll(array('order' => 'name'));

        $persList = CHtml::listData($pers, 'id','name');


        $this->render('payments_update', array('model' => $model, 'pers' => $persList));
    }

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

    public function getUserName($id){
        $pers = Pers::model()->findByPk($id);
        return $pers->name;
    }
}