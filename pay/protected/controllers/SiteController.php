<?php

class SiteController extends BaseController
{
    private $_model;
    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function loadModel($id = 0)
    {
        if ($this->_model === null) {
            if (!empty($id)) {
                $condition = 'AND id = ' . $id;

                $this->_model = Pers::model()->find(array(
                    'condition' => 'del=:del '. $condition,
                    'params' => array(':del' => 0)
                ));
            } else {
                $this->_model = new Pers;
            }


            if ($this->_model === null)
                throw new CHttpException(404, 'Запрашиваемая страница не существует.');
        }
        return $this->_model;
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        if(Yii::app()->user->isGuest){
            $this->redirect(Yii::app()->createUrl('site/login'));
        }

        $criteria = new CDbCriteria();
        $criteria->condition = 'del=0 AND cid = '. $this->getUserCompany();
        $criteria->order = 'id ASC';

        $pages = new CPagination(Pers::model()->count());
        $pages->pageSize = 10;
        $pages->applyLimit($criteria);

        $dataProvider = new CActiveDataProvider('Pers', array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 50,
            ),

        ));

        $this->render('pers', array('dataProvider' => $dataProvider, 'pages' => $pages));
    }

    public function actionDelete($id)
    {
        $model = $this->loadModel($id);
        if ($model === null)
            throw new CHttpException(404, 'Запрашиваемая страница не существует.');

        $model->delete();
        $this->actionIndex();
        Yii::app()->end();

    }

    public function actionUpdate($id = 0)
    {
        if(!empty($id)){
            $model = $this->loadModel($id);
        }else{
            $model = new Pers();
        }

        if (isset($_POST['Pers'])) {
            $model->attributes = $_POST['Pers'];

            $model->cid = $this->getUserCompany();
            $model->date = date ('Y-m-d', strtotime($model->date));

            $model->date_end = date ('Y-m-d', strtotime($model->date_end));



            if ($model->validate()) {
                if ($model->save()) {
                    CVarDumper::dump($model->getErrors(), 5678, true);
                    $this->actionIndex();
                    Yii::app()->end();
                }
            }
        }

        $model->date = date ('d-m-Y', strtotime($model->date));
        $model->date_end = date ('d-m-Y', strtotime($model->date_end));

        $company = Companies::model()->findAll(array('order' => 'company_name'));
        $companyList = CHtml::listData($company, 'id','company_name');

        $countries = Countries::model()->findAll(array('order' => 'name'));
        $countriesList = CHtml::listData($countries, 'id','name');

        $docs = Docs::model()->findAll(array('order' => 'name'));
        $docsList = CHtml::listData($docs, 'code','name');

        $region = Region::model()->findAll(array('order' => 'name'));
        $regionList = CHtml::listData($region, 'id','name');

        $ifns = Ifns::model()->findAll(array('order' => 'name'));
        $ifnsList = CHtml::listData($ifns, 'code','name');

        $this->render('pers_update', array('model' => $model, 'company' => $companyList, 'countries' => $countriesList, 'docs' => $docsList, 'region' => $regionList, 'ifns' => $ifnsList));
    }

    public function getCompanyName($id){
        $comapny = Companies::model()->findByPk($id);
        return $comapny->company_name;
        //$comapny->company_name;
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the login page
     */
    public function actionLogin()
    {
        $form = new Accounts();

        // Проверяем является ли пользователь гостем
        // ведь если он уже зарегистрирован - формы он не должен увидеть.
        if (!Yii::app()->user->isGuest) {
            throw new CException('Вы уже авторизованы!');
        } else {
            if (!empty($_POST['Accounts'])) {
                $form->attributes = $_POST['Accounts'];
                // Проверяем правильность данных
                if($form->validate('login')) {
                    $identity = new UserIdentity($form->login, $form->password);
                    $identity->authenticate();
                    Yii::app()->user->login($identity);

                    // если всё ок - кидаем на главную страницу
                    $this->redirect(Yii::app()->homeUrl);
                }
            }
            $this->render('login', array('model' => $form));
        }
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function getUserCompany(){
        $c = Accounts::model()->findByPk(Yii::app()->user->getId());
        return $c->cid;
    }

    public function getUserCompanyName(){
        $c = Accounts::model()->findByPk(Yii::app()->user->getId());
        $d = Companies::model()->findByPk($c->cid);
        return $d->company_name;
    }


    public function actionReport($print=0)
    {
        $criteria = new CDbCriteria();

        $criteria->order = 'date_end ASC';

        $cid = $this->getUserCompany();

        $criteria->condition = 'date_end >= DATE_SUB(CURDATE(), INTERVAL 5 DAY) AND date_end <= CURDATE() AND cid='.$cid;

        $pages = new CPagination(Pers::model()->count());
        $pages->pageSize = 10;
        $pages->applyLimit($criteria);

        $dataProvider=new CActiveDataProvider('Pers', array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 50,
            ),
        ));

        $company = Companies::model()->findAll(array('order' => 'company_name'));
        $companyList = CHtml::listData($company, 'id','company_name');

        $countries = Countries::model()->findAll(array('order' => 'name'));
        $countriesList = CHtml::listData($countries, 'id','name');

        $docs = Docs::model()->findAll(array('order' => 'name'));
        $docsList = CHtml::listData($docs, 'code','name');

        $region = Region::model()->findAll(array('order' => 'name'));
        $regionList = CHtml::listData($region, 'id','name');

        $ifns = Ifns::model()->findAll(array('order' => 'name'));
        $ifnsList = CHtml::listData($ifns, 'code','name');

        if(!$print)
            $this->render('report', array('dataProvider'=>$dataProvider, 'pages' => $pages, 'company' => $companyList, 'countries' => $countriesList, 'docs' => $docsList, 'region' => $regionList, 'ifns' => $ifnsList));
        else
            $this->renderPartial('report', array('dataProvider'=>$dataProvider, 'pages' => $pages, 'company' => $companyList, 'countries' => $countriesList, 'docs' => $docsList, 'region' => $regionList, 'ifns' => $ifnsList));
    }

    public function actionOverdue($print=0)
    {
        $criteria = new CDbCriteria();

        $criteria->order = 'date_end ASC';

        $cid = $this->getUserCompany();
        $criteria->condition = 'date_end > CURDATE() AND cid='.$cid;

        $pages = new CPagination(Pers::model()->count());
        $pages->pageSize = 10;
        $pages->applyLimit($criteria);


        $dataProvider=new CActiveDataProvider('Pers', array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 50,
            ),

        ));

        $company = Companies::model()->findAll(array('order' => 'company_name'));
        $companyList = CHtml::listData($company, 'id','company_name');

        $countries = Countries::model()->findAll(array('order' => 'name'));
        $countriesList = CHtml::listData($countries, 'id','name');

        $docs = Docs::model()->findAll(array('order' => 'name'));
        $docsList = CHtml::listData($docs, 'code','name');

        $region = Region::model()->findAll(array('order' => 'name'));
        $regionList = CHtml::listData($region, 'id','name');

        $ifns = Ifns::model()->findAll(array('order' => 'name'));
        $ifnsList = CHtml::listData($ifns, 'code','name');

        if(!$print)
            $this->render('override', array('dataProvider'=>$dataProvider, 'pages' => $pages, 'company' => $companyList, 'countries' => $countriesList, 'docs' => $docsList, 'region' => $regionList, 'ifns' => $ifnsList));
        else
            $this->renderPartial('override', array('dataProvider'=>$dataProvider, 'pages' => $pages, 'company' => $companyList, 'countries' => $countriesList, 'docs' => $docsList, 'region' => $regionList, 'ifns' => $ifnsList));

    }

    public function actionPrintReport(){
        $this->actionReport(1);
    }

    public function actionPrintOverride(){
        $this->actionOverdue(1);
    }
}