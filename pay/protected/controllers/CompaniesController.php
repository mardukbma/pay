<?php

class CompaniesController extends BaseController
{
    private $_model;
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

    public function loadModel($id = 0)
    {
        if($this->_model===null)
        {
            if(!empty($id)){
                $condition = 'id = ' . $id;
            }
            $this->_model=Companies::model()->find(array(), $condition);

            if($this->_model===null)
                throw new CHttpException(404,'Запрашиваемая страница не существует.');
        }
        return $this->_model;
    }

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
        $criteria = new CDbCriteria();

        $criteria->order = 'company_name ASC';

        $pages = new CPagination(Companies::model() -> count());
        $pages->pageSize = 10;
        $pages->applyLimit($criteria);

        $dataProvider=new CActiveDataProvider('Companies', array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 10,
            ),

        ));

        $this->render('companies', array('dataProvider'=>$dataProvider, 'pages' => $pages));
	}

    public function actionDelete($id)
    {
        $model = $this->loadModel($id);
        if($model===null)
            throw new CHttpException(404,'Запрашиваемая страница не существует.');

        $model->delete();
        $this->actionIndex();
        Yii::app()->end();

    }

    public function actionUpdate($id = 0)
    {
        if(!empty($id)){
            $model = $this->loadModel($id);
        }else{
            $model = new Companies();
        }

        if (isset($_POST['Companies'])) {
            $model->attributes = $_POST['Companies'];
            if ($model->validate()) {
                if ($model->save()) {
                    $this->actionIndex();
                    Yii::app()->end();
                }
            } else {
                //here you can send an error message via FLASH or you can debug what the exact error is like below:
                CVarDumper::dump($model->getErrors(), 5678, true);
                Yii::app()->end();
            }

        }


        $this->render('companies_update', array('model' => $model));
    }

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
}