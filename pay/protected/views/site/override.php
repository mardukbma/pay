<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>
<div id="mainmenu">
		<?php $this->widget('zii.widgets.CMenu',array(
        'items'=>array(
            array('label'=>'Патенты со сроком менее 5 дней', 'url'=>array('/site/report')),
            array('label'=>'Просроченные патенты', 'url'=>array('/site/overdue')),
        ),
)); ?>
</div><!-- mainmenu -->
<br/>
<h2>Просроченные патенты</h2>
<?php
$widget = $this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $dataProvider,
    'enablePagination' => true,
    'columns' => array(
        'id',
        'name',
        array(
            'name' => 'type',
            'value' => '$data->getTypeName($data->type)',
        ),
        'doc_num',
        array(
            'name' => 'nationality',
            'value' => '$data->getNationalName($data->nationality)',
        ),
        'patent_num',
        array(
            'name' => 'date',
            'value' => 'date("d-m-Y", strtotime($data->date))',
        ),
        array(
            'name' => 'date_end',
            'value' => 'date("d-m-Y", strtotime($data->date_end))',
        ),
        array(
            'name' => 'Дата очередного продления',
            'value' => '$data->getPayDate($data->id)',
        ),
        'phone',
        /* array(
        'name'=>'Компания',
        'value'=>'$data->getCompanyName($data->cid)',
        ),*/
        'ifns',
//'region',
// 'address',
        'comment',
        array(
            'class' => 'CButtonColumn',
            'template' => '{payments} {update} {delete}',
            'buttons' => array
            (
                'payments' => array
                (
                    'label' => '[-]',
                    'options' => array(
                        'title' => 'Реестр платежей'
                    ),
                    'url' => 'Yii::app()->createUrl("payments/index", array("id"=>$data->id))',
//'click'=>'function(){alert("Going down!");}',
                ),
            ),
        ),
    ),
));
echo CHtml::button('Печать', array('onclick' => 'js:document.location.href="PrintOverride"'));
?>
