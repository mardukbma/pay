<?php
/**
 * Created by PhpStorm.
 * User: marduk
 * Date: 11.08.2015
 * Time: 16:26
 */
?>
<h2>Редактирование</h2>
<div class="form">
    <?php $form = $this->beginWidget('CActiveForm'); ?>

    <?php echo $form->errorSummary($model); ?>
    <?php if(!empty($model->id)){ ?>
        <div class="row">
            <?php echo $form->hiddenField($model, 'id') ?>
            <?php echo $form->error($model, 'id') ?>
        </div>
    <?php }?>

    <div class="row">
        <?php echo $form->label($model, 'name'); ?>
        <?php echo $form->textField($model, 'name'); ?>
        <?php echo $form->error($model,'name'); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'type'); ?>
        <?php echo $form->dropDownList($model, 'type', $docs);  ?>
        <?php echo $form->error($model,'type'); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'doc_num'); ?>
        <?php echo $form->textField($model, 'doc_num'); ?>
        <?php echo $form->error($model,'doc_num'); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'nationality'); ?>
        <?php echo $form->dropDownList($model, 'nationality', $countries);  ?>
        <?php echo $form->error($model,'nationality'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'patent_num'); ?>
        <?php echo $form->textField($model, 'patent_num'); ?>
        <?php echo $form->error($model,'patent_num'); ?>
    </div>


    <div class="row">
        <?php echo $form->label($model, 'date'); ?>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker',array(
            'name'=>'Date',
            'language'=>'ru',
            'model'=>$model,
            'attribute'=>'date',

            // additional javascript options for the date picker plugin
            'options'=>array(
                'showAnim'=>'fold',
                'dateFormat'=>'dd-mm-yy',
                'showButtonPanel' => 'true'
            ),
            'htmlOptions'=>array(
                'style'=>'height:20px;'
            ),
        )); ?>
        <?php echo $form->error($model,'date'); ?>
    </div>
   <!-- <div class="row">
        <?php /*echo $form->label($model, 'date_end'); */?>
        <?php
/*        $this->widget('zii.widgets.jui.CJuiDatePicker',array(
            'name'=>'Date_end',
            'language'=>'ru',
            'model'=>$model,
            'attribute'=>'date_end',

            // additional javascript options for the date picker plugin
            'options'=>array(
                'showAnim'=>'fold',
                'dateFormat'=>'dd-mm-yy',
                'showButtonPanel' => 'true'
            ),
            'htmlOptions'=>array(
                'style'=>'height:20px;'
            ),
        )); */?>
        <?php /*echo $form->error($model,'date_end'); */?>
    </div>-->

    <div class="row">
        <?php echo $form->labelEx($model,'phone'); ?>
        <?php
        $this->widget('CMaskedTextField', array(
            'model' => $model,
            'attribute' => 'phone',
            'mask' => '+7-999-999-9999',
            'placeholder' => '*',
            'completed' => 'function(){console.log("ok");}',
        ));
        ?>
        <?php echo $form->error($model,'phone'); ?>
    </div>

    <div class="row">
        <?php //echo $form->dropDownList($model, 'cid', $company);  ?>
        <?php echo $form->hiddenField($model, 'cid'); ?>
        <?php echo $form->error($model,'cid'); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'region'); ?>
        <?php echo $form->dropDownList($model, 'region', $region);  ?>
        <?php echo $form->error($model,'region'); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'address'); ?>
        <?php echo $form->textField($model, 'address'); ?>
        <?php echo $form->error($model,'address'); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'ifns'); ?>
        <?php echo $form->dropDownList($model, 'ifns', $ifns);  ?>
        <?php echo $form->error($model,'ifns'); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'comment'); ?>
        <?php echo $form->textArea($model, 'comment'); ?>
        <?php echo $form->error($model,'comment'); ?>
    </div>



    <div class="row submit">
       <?php echo CHtml::submitButton("Сохранить"); ?>
    </div>
    <?php $this->endWidget(); ?></div>
<!-- form -->