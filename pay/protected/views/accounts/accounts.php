<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;

echo CHtml::button('Добавить', array('onclick' => 'js:document.location.href="update"'));
$widget = $this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider'=>$dataProvider,
    'enablePagination' => true,
    'columns'=>array(
        'id',
        'name',
        array(
            'name'=>'Компания',
            'value'=>'$data->getCompanyName($data->cid)',
        ),
        'login',
        'email',
        array(
            'class' => 'CButtonColumn',
            'template' => '{update}{delete}',
        ),
    ),
));
?>