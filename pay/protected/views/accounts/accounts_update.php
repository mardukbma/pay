<?php
/**
 * Created by PhpStorm.
 * User: marduk
 * Date: 11.08.2015
 * Time: 16:26
 */
?>
<h2>New/Edit Pers</h2>
<div class="form">
    <?php $form = $this->beginWidget('CActiveForm'); ?>

    <?php echo $form->errorSummary($model); ?>
    <?php if(!empty($model->id)){ ?>
        <div class="row">
            <?php echo $form->hiddenField($model, 'id') ?>
            <?php echo $form->error($model,'id'); ?>
        </div>
    <?php }?>
    <div class="row">
        <?php echo $form->label($model, 'name'); ?>
        <?php echo $form->textField($model, 'name'); ?>
        <?php echo $form->error($model,'name'); ?>
    </div>
    <div class="row">
        <?php //echo $form->label($model, 'cid'); ?>
        <?php echo $form->hiddenField($model, 'cid');  ?>
        <?php echo $form->error($model,'cid'); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'login'); ?>
        <?php echo $form->textField($model, 'login'); ?>
        <?php echo $form->error($model,'login'); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'password'); ?>
        <?php echo $form->passwordField($model, 'password'); ?>
        <?php echo $form->error($model,'password'); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'email'); ?>
        <?php echo $form->textField($model, 'email'); ?>
        <?php echo $form->error($model,'email'); ?>
    </div>

    <div class="row submit">
       <?php echo CHtml::submitButton("Сохранить"); ?>
    </div>
    <?php $this->endWidget(); ?></div>
<!-- form -->