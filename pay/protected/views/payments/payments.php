<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
if(Yii::app()->user->getId() == 1){
    echo CHtml::button('Добавить', array('onclick' => 'js:document.location.href="update"'));
    $button = array(
        'class' => 'CButtonColumn',
        'template' => '{update}{delete}',
    );
}else{
    $button = array(
        'class' => 'CButtonColumn',
        'template' => '',
    );
}


$widget = $this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider'=>$dataProvider,
    'enablePagination' => true,
    'columns'=>array(
        'id',
        array(
            'name'=>'pers_id',
            'value'=>'$data->getUserName($data->pers_id)',
        ),
        'sum',
        array(
            'name'=>'date',
            'value'=>'date("d-m-Y", strtotime($data->date))',
        ),
        array(
            'name'=>'payed_till',
            'value'=>'date("d-m-Y", strtotime($data->payed_till))',
        ),
        $button
    ),
));
?>