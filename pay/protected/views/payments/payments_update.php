<?php
/**
 * Created by PhpStorm.
 * User: marduk
 * Date: 11.08.2015
 * Time: 16:26
 */
?>
<h2>Справочник ифнс</h2>
<div class="form">
    <?php $form = $this->beginWidget('CActiveForm'); ?>

    <?php echo $form->errorSummary($model); ?>
    <?php if(!empty($model->id)){ ?>
        <div class="row">
            <?php echo $form->hiddenField($model, 'id') ?>
            <?php echo $form->error($model,'id'); ?>
        </div>
    <?php }?>
    <div class="row">
        <?php echo $form->label($model, 'pers_id'); ?>
        <?php echo $form->dropDownList($model, 'pers_id', $pers);  ?>
        <?php echo $form->error($model,'pers_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'sum'); ?>
        <?php echo $form->textField($model, 'sum'); ?>
        <?php echo $form->error($model,'sum'); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'date'); ?>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker',array(
            'name'=>'Date',
            'language'=>'ru',
            'model'=>$model,
            'attribute'=>'date',

            // additional javascript options for the date picker plugin
            'options'=>array(
                'showAnim'=>'fold',
                'dateFormat'=>'dd-mm-yy',
                'showButtonPanel' => 'true'
            ),
            'htmlOptions'=>array(
                'style'=>'height:20px;'
            ),
        )); ?>
        <?php echo $form->error($model,'date'); ?>
    </div>
<!--    <div class="row">
        <?php /*echo $form->label($model, 'exported'); */?>
        <?php /*echo $form->textField($model, 'exported'); */?>
        <?php /*echo $form->error($model,'exported'); */?>
    </div>-->
<!--    <div class="row">
        <?php /*echo $form->label($model, 'payed_till'); */?>
        <?php /*echo $form->textField($model, 'payed_till'); */?>
        <?php /*echo $form->error($model,'payed_till'); */?>
    </div>-->

    <div class="row submit">
       <?php echo CHtml::submitButton("Сохранить"); ?>
    </div>
    <?php $this->endWidget(); ?></div>
<!-- form -->