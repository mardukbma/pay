<?php
/**
 * Created by PhpStorm.
 * User: marduk
 * Date: 11.08.2015
 * Time: 16:26
 */
?>
<h2>New/Edit Pers</h2>
<div class="form">
    <?php $form = $this->beginWidget('CActiveForm'); ?>

    <?php echo $form->errorSummary($model); ?>
    <?php if(!empty($model->id)){ ?>
        <div class="row">
            <?php echo $form->hiddenField($model, 'id') ?>
            <?php echo $form->error($model,'id'); ?>
        </div>
    <?php }?>
    <div class="row">
        <?php echo $form->label($model, 'company_name'); ?>
        <?php echo $form->textArea($model, 'company_name'); ?>
        <?php echo $form->error($model,'company_name'); ?>
    </div>

    <div class="row submit">
       <?php echo CHtml::submitButton("Сохранить"); ?>
    </div>
    <?php $this->endWidget(); ?></div>
<!-- form -->