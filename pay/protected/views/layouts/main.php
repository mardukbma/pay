<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print">
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection">
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css">

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">

	<div id="header">
		<div id="logo">
            <img src="/images/E_PAY_SYSTEMS.png" width="100">
            <p style="font-size: 16px; padding-top:10px;">Компания: <?php echo Pers::getUserCompanyName() ?></p>
        </div>
	</div><!-- header -->
<?php

if(Yii::app()->user->getId() == 1) $is_admin = true; else $is_admin = false; ?>
	<div id="mainmenu">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'Иностранцы', 'url'=>array('/site/index'), 'visible'=>!Yii::app()->user->isGuest),
                array('label'=>'Сотрудники', 'url'=>array('/accounts/index'), 'visible'=>!Yii::app()->user->isGuest),
                array('label'=>'Платежи', 'url'=>array('/payments/index'), 'visible'=>!Yii::app()->user->isGuest),
                array('label'=>'Компании', 'url'=>array('/companies/index'), 'visible'=>$is_admin),
                array('label'=>'ИФНС', 'url'=>array('/ifns/index'), 'visible'=>!Yii::app()->user->isGuest),
                array('label'=>'Отчеты', 'url'=>array('/site/report'), 'visible'=>!Yii::app()->user->isGuest),
				array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		)); ?>
	</div><!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

    <?php $count = Pers::model()->getCount(); ?>
    <?php if(!empty($count)){ ?>
    <div style="padding:20px 0 0 20px;">
        <p>У вас <?php echo $count['r'] ?> патентов ожидающих оплаты, <?php echo $count['e'] ?> просроченных оплат.</p>
    </div>
    <?php } ?>
	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
