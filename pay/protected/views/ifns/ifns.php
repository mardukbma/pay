<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;

echo CHtml::button('Добавить', array('onclick' => 'js:document.location.href="update"'));
$widget = $this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider'=>$dataProvider,
    'enablePagination' => true,
    'columns'=>array(
        'code',
        'name',
        'fullname',
        'kpp',
        'inn',
        'address',
        'phone',
        'mail',
        'okpo',
        'bank',
        'bik',
        'korr',
        'num',
        array(
            'class' => 'CButtonColumn',
            'template' => '{update}{delete}',
        ),
    ),
));
?>
