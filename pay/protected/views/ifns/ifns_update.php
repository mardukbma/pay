<?php
/**
 * Created by PhpStorm.
 * User: marduk
 * Date: 11.08.2015
 * Time: 16:26
 */
?>
<h2>Справочник ифнс</h2>
<div class="form">
    <?php $form = $this->beginWidget('CActiveForm'); ?>

    <?php echo $form->errorSummary($model); ?>
    <div class="row">
        <?php echo $form->label($model, 'code'); ?>
        <?php echo $form->textField($model, 'code') ?>
        <?php echo $form->error($model,'code'); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'name'); ?>
        <?php echo $form->textField($model, 'name') ?>
        <?php echo $form->error($model,'name'); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'fullname'); ?>
        <?php echo $form->textField($model, 'fullname') ?>
        <?php echo $form->error($model,'fullname'); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'kpp'); ?>
        <?php echo $form->textField($model, 'kpp') ?>
        <?php echo $form->error($model,'kpp'); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'address'); ?>
        <?php echo $form->textField($model, 'address') ?>
        <?php echo $form->error($model,'address'); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'phone'); ?>
        <?php echo $form->textField($model, 'phone') ?>
        <?php echo $form->error($model,'phone'); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'mail'); ?>
        <?php echo $form->textField($model, 'mail') ?>
        <?php echo $form->error($model,'mail'); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'okpo'); ?>
        <?php echo $form->textField($model, 'okpo') ?>
        <?php echo $form->error($model,'okpo'); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'bank'); ?>
        <?php echo $form->textField($model, 'bank') ?>
        <?php echo $form->error($model,'bank'); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'bik'); ?>
        <?php echo $form->textField($model, 'bik') ?>
        <?php echo $form->error($model,'bik'); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'korr'); ?>
        <?php echo $form->textField($model, 'korr') ?>
        <?php echo $form->error($model,'korr'); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'num'); ?>
        <?php echo $form->textField($model, 'num') ?>
        <?php echo $form->error($model,'num'); ?>
    </div>
    <div class="row submit">
       <?php echo CHtml::submitButton("Сохранить"); ?>
    </div>
    <?php $this->endWidget(); ?></div>
<!-- form -->